﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MatematikWebProje.Models;

namespace MatematikWebProje.Controllers
{
    public class KonuanlatimsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Konuanlatims
        public async Task<ActionResult> Index()
        {
            var konuanlatims = db.Konuanlatims.Include(k => k.Dersler);
            return View(await konuanlatims.ToListAsync());
        }

        // GET: Konuanlatims/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Konuanlatim konuanlatim = await db.Konuanlatims.FindAsync(id);
            if (konuanlatim == null)
            {
                return HttpNotFound();
            }
            return View(konuanlatim);
        }

        // GET: Konuanlatims/Create
        public ActionResult Create()
        {
            ViewBag.DersID = new SelectList(db.Derslers, "DersID", "DersAdi");
            return View();
        }

        // POST: Konuanlatims/Create
        // Aşırı gönderim saldırılarından korunmak için, lütfen bağlamak istediğiniz belirli özellikleri etkinleştirin, 
        // daha fazla bilgi için https://go.microsoft.com/fwlink/?LinkId=317598 sayfasına bakın.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "KonuID,KonuAdi,DersID")] Konuanlatim konuanlatim)
        {
            if (ModelState.IsValid)
            {
                db.Konuanlatims.Add(konuanlatim);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.DersID = new SelectList(db.Derslers, "DersID", "DersAdi", konuanlatim.DersID);
            return View(konuanlatim);
        }

        // GET: Konuanlatims/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Konuanlatim konuanlatim = await db.Konuanlatims.FindAsync(id);
            if (konuanlatim == null)
            {
                return HttpNotFound();
            }
            ViewBag.DersID = new SelectList(db.Derslers, "DersID", "DersAdi", konuanlatim.DersID);
            return View(konuanlatim);
        }

        // POST: Konuanlatims/Edit/5
        // Aşırı gönderim saldırılarından korunmak için, lütfen bağlamak istediğiniz belirli özellikleri etkinleştirin, 
        // daha fazla bilgi için https://go.microsoft.com/fwlink/?LinkId=317598 sayfasına bakın.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "KonuID,KonuAdi,DersID")] Konuanlatim konuanlatim)
        {
            if (ModelState.IsValid)
            {
                db.Entry(konuanlatim).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.DersID = new SelectList(db.Derslers, "DersID", "DersAdi", konuanlatim.DersID);
            return View(konuanlatim);
        }

        // GET: Konuanlatims/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Konuanlatim konuanlatim = await db.Konuanlatims.FindAsync(id);
            if (konuanlatim == null)
            {
                return HttpNotFound();
            }
            return View(konuanlatim);
        }

        // POST: Konuanlatims/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Konuanlatim konuanlatim = await db.Konuanlatims.FindAsync(id);
            db.Konuanlatims.Remove(konuanlatim);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
