namespace MatematikWebProje.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class konuvideoekle : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.KonuVideo",
                c => new
                    {
                        KonuVideoID = c.Int(nullable: false, identity: true),
                        Link = c.String(nullable: false),
                        KonuID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.KonuVideoID)
                .ForeignKey("dbo.Konular", t => t.KonuID, cascadeDelete: true)
                .Index(t => t.KonuID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.KonuVideo", "KonuID", "dbo.Konular");
            DropIndex("dbo.KonuVideo", new[] { "KonuID" });
            DropTable("dbo.KonuVideo");
        }
    }
}
