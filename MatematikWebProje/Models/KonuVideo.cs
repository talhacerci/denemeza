﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MatematikWebProje.Models
{
    [Table("KonuVideo")]
    public class KonuVideo
    {
        [Key]
        public int KonuVideoID { get; set; }
        [Required]
        public string Link { get; set; }
        public int KonuID { get; set; }
        [ForeignKey("KonuID")]
        public Konuanlatim Konulars { get; set; }
    }
}