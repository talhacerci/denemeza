﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MatematikWebProje.Models
{
    [Table("Dersler")]

    public class Dersler
    {
        [Key]
        public int DersID { get; set; }
        [Required, MaxLength(200)]
        public string DersAdi { get; set; }
        public virtual List<Konuanlatim> Konuanlatims { get; set; }
    }
}