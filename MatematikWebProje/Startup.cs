﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MatematikWebProje.Startup))]
namespace MatematikWebProje
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
